// console.log("Hello world!");

// SECTION JSON Objects

/*
	-JSON > JavaScript Object Notation
	-common use is to read data from a web server and display the data in a web page
	-Features of JSON:
		-lightweight data-interchange format
		-easy to read and tight
		-easy for machine to parse and generate
		-
*/

// JSON Objects
// - JSON also use the "key/value pairs" just like the object properties in JavaScript
// -"Key/property" name requires to be enclosed with double quotes. (like string)

/*
	Syntax:
	{
		"propertyA" : "valueA",
		"propertyB" : "value"
	}
*/


// Example of JSON Object
/*
{
	"city": "Quezon City",
	"province": "Metro Manila",
	"country": "Philippines"
}


// Arrays of JSON Object

"cities" : [
	{
		"city" : "Quezon City",
		"province": "Metro Manila",
		"country": "Philippines"
	}
	{
		"city" : "Manila City",
		"province": "Metro Manila",
		"country": "Philippines"
	}
	{
		"city" : "Makati City",
		"province": "Metro Manila",
		"country": "Philippines"
	}
]
*/

console.log('--------------');

// SECTION
// JavaScript Array of Objects
let batchesArr = [
	{
		batchName: "Batch 218",
		schedule: "Part Time"
	},
	{
		batchName: "Batch 219",
		schedule: "Full Time"
	}
]

console.log(batchesArr);


// The "stringify" method used to cinvert javascipt obects into a string

console.log('Result of stringify method: ');
console.log(JSON.stringify(batchesArr));


let data = JSON.stringify({
	name: "John",
	age: 31,
	address: {
		city: "Manila",
		country: "Philippines"

	}

})

console.log(data);
//global variables


/*
let firstName = prompt("Enter your first name: ");
let lastName = prompt("Enter your last name: ");
let email = prompt("Enter your email: ");
let password = prompt("Enter your password: ");

let otherData = JSON.stringify({
	firsName: firstName,
	lastName: lastName,
	email: email,
	password: password

})

console.log(otherData);
*/



// SECTION - Converting Stringify JSON into JavaScript Objects

let batchesJSON = `[
	{
		"batchName": "Batch 218",
		"schedule": "Part Time"

	},
	{
		"batchName": "Batch 219",
		"schedule": "Full Time"

	}

]`

console.log("bathcesJSON content: ")
console.log(batchesJSON);

//JSON.parse method converts JSON Objects into JavaScript Objects
console.log("Result from parse method:")
console.log(JSON.parse(batchesJSON));

// store in a variable
let parseBatches = JSON.parse(batchesJSON);
console.log(parseBatches);
console.log(parseBatches[0]);
console.log(parseBatches[0].batchName);

let stringifiedObject = `{
	"name": "John",
	"age": 31,
	"address": {
		"city": "Manila",
		"country": "Philippines"
	}
}`

console.log(stringifiedObject);
console.log(JSON.parse(stringifiedObject));























